# <div align='center'>Github-Repos-Viewer</div>

A React Web Application written with an Express and Node.js Backend, to view the public Github Repos of a Github User.

## Requisites

* Node.js
* NPM (Node Package Manager)
* Express.js
* Nodemon (In case you want to develop the project further.)

## Usage

First off, clone the git repository to your system or download the zip file containing the files.

```bash
git clone https://github.com/deve-sh/Github-Repos-Viewer.git
cd Github-Repos-Viewer
```

Then run `npm install` to install all the dependencies of the project.

To start the web server, run `npm start`. In order to start a web server that automatically refreshes whenever there is a change in the source code, install **Nodemon** (Listed as a developer dependency in package.json) and run `npm run autostart` to start the web server with Nodemon. Then visit your server in the web browser at the port that shows up in the console.

## Development

The React Web Application is stored in the **public/** folder. To edit the React App, just edit the **App.js** file, the styles are listed in the styles.css file. Other external dependencies (Static HTML dependencies) include : 

- Bootstrap
- Font Awesome
- Babel
- React

For any changes you may make to the project that you think makes it better. Just submit a pull request to this repo.

## Support and License

For any support or questions regarding the project, simply raise an issue in the repository, or [email me](mailto:devesh2027@gmail.com).

The Project has no license attributed to it.