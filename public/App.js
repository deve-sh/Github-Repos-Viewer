/*
    React File.
*/

const projectName = "Github Repos Viewer";

// Button Component

const Button = (props) => <button type={props.type} className={props.className}>{props.label}</button>;

Button.defaultProps = {
    type:"",
    className:"btn",
    label: "Do Something"
}

// User Card Component

const Usercard = (props) => {
    return (
    <div id='user' style={{textAlign : 'center'}}>
        <img src={props.imgurl} alt={props.username.toString() + "'s Profile Photo"}/>
        <br/>
        <br/>
        <a href={props.link} target="_blank">
            <span className='username'>
                <h3>{props.username}</h3>
            </span>
        </a>
    </div>);
}

Usercard.defaultProps = {
    imgurl : "",
    username: "",
    link: "javascript:void(0)"
};

// Repository Component

const Repo = (props) => {
    return (
        <div className='repo' key={props.id}>
            <a href={props.link} target="_blank">
                <div className='reponame'>{props.repoName}</div>
                <div className='repodesc'>{props.repoDesc}</div>
            </a>
                <div className='row'>
                    <div className='col col-xs-3' style={{textAlign: 'left'}}>
                        <span className='repostars'>
                            <i className="fas fa-star"></i> &nbsp;&nbsp;{props.starCount}
                        </span>
                    </div>
                    <div className='repolang col col-xs-9' style={{textAlign: 'right'}}>{props.repoLang}</div>
                </div>
        </div>
    );
}

Repo.defaultProps = {
    id: 0,
    repoName : "",
    repoDesc : "",
    starCount: 0,
    repoLang : "",
    link: "javascript:void(0)"
}

/*
    User Details Component. Does all the heavy lifting.
*/

class UserDetails extends React.Component{
    constructor(props){
        super(props);
        this.getUser = this.getUser.bind(this);
        
        this.state = {
            username : "",
            retreived : false,
            torender : "",
            usercard : "",
            showuser : false
        }

        // Necessary Vars

        this.torender = "";
        this.usercard = "";
    }

    getUser(){
        // Function to run an XMLHTTPRequest to fetch the details of the user provided and render them.
        let xhr = new XMLHttpRequest();
        let endpoint = `https://api.github.com/users/${this.props.username}/repos`;

        xhr.open('GET', endpoint);
        
        xhr.send();

        xhr.onload = () => {
            // Function
            
            if(xhr.status === 200)
            {
                // If the Request worked.

                let repos = JSON.parse(xhr.responseText);

                if(Array.isArray(repos) && repos.length>0){
                    // If the thing is valid.

                    if(this.state.username != this.props.username){ 
                        // Checking if the username passed is new.

                        this.torender = repos.map(repo => <Repo key={repo.id} repoName={repo.name} repoDesc={repo.description} langColor={"#000000"} repoLang = {repo.language} starCount={repo.stargazers_count} link={repo.html_url}/>);
                        this.usercard = <Usercard username = {repos[0].owner.login} imgurl={repos[0].owner.avatar_url} link={repos[0].owner.html_url}/>;

                        this.setState({
                            username : this.props.username,
                            retreived : true,
                            torender : this.torender,
                            showuser : true,
                            usercard : this.usercard
                        });     // Reload the Component.
                    }
                }
                else{
                    if(repos.message == "Not Found".toLowerCase()){
                        this.torender = "Could not find a user with that username.";
                        this.setState({
                            retreived: false,
                            torender : "",
                            showuser: false,
                            usercard: ""
                        }); // Re-render the component to print the
                    }
                }
            }
            else{
                this.torender = "Could not find a user with that username / API might have crashed!";

                this.setState({
                    retreived: false,
                    torender: this.torender,
                    username : this.props.username,
                    usercard : "",
                    showuser : false
                });
            }
    
        }
    }

    render(){
        if(this.props.username!==""){

            if(this.props.username !== this.state.username)
            {
                // If a new user has been asked for.
                this.getUser();
            }

            return (
                <div id='userdetails'>
                    <div style={{textAlign:'center'}}>{this.state.showuser === true?this.usercard:""}</div>
                    <br/>
                    <br/>
                    {this.state.torender}
                </div>
            );
        }
        else{
            return ("");
        }
    }
}

/* Header Component */

class Header extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
        <div id='header'>
           <h1 style={{textAlign:'center'}} className='text text-fluid'>
                {projectName}
           </h1>
           <br/>
           <form onSubmit={this.props.userUpdater} style={{textAlign:'center'}}>
                <input className='form-control' placeholder={"Username"}/>
                <br/>
                <Button type="submit" label="Submit" className="submitbutton"/>
           </form>
        </div>);
    }
}

/* App Presentational Component */

class App extends React.Component{
    constructor(props){
        super(props);
        this.userUpdater = this.userUpdater.bind(this);

        this.state = {
            username : ""
        }
    }

    userUpdater(e){
        e.preventDefault();
        let enteredUsername = e.target.children[0].value;
        this.setState({username : enteredUsername});
    }

    render(){
        return (
        <React.Fragment>
            <Header userUpdater={this.userUpdater}/>
            <div style={{padding: "1rem"}}>
                {(this.state.username === "")?<div style={{textAlign:'center'}}><br/>Enter a Github username.</div>:<UserDetails username={this.state.username}/>}
            </div>
        </React.Fragment>
        );
    }
}

// Render

ReactDOM.render(<App/>,document.querySelector('#root'));