/*
    Express File to start a web server on PORT 8080 and serve contents from the public directory.
*/

const express = require('express');
const app = express();

const PORT = process.env.port||8080;    // You could change the port number in the environment file.

app.listen(PORT, (req,res) => {
    console.log("Listening at " + PORT);
});

// Serve the react project on the / route.

app.use(express.static(__dirname + '/public'));
